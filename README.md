# POPCHEF - Technical test

## Install

Make sure to install [Docker](https://docs.docker.com/get-docker/).

```sh
> docker compose up
```

- Frontend is running on default port 3000
- Backend is running on default port 4000 (from your browser, you can access Apollo Studio on this port to test your GraphQL API)


# Projet de gestion des produits et ingrédients - Résumé

Ce projet consistait à développer une application de gestion des produits et ingrédients à l'aide de React, Ant Design, GraphQL et MongoDB. J'ai réussi à mettre en œuvre les fonctionnalités suivantes :

1. Création, modification et suppression de produits : J'ai créé des formulaires et des pages permettant de créer, mettre à jour et supprimer des produits. Les utilisateurs peuvent saisir le nom, la description et les ingrédients associés à chaque produit.

2. Création, modification et suppression d'ingrédients : De la même manière, j'ai créé des formulaires et des pages pour gérer les ingrédients. Les utilisateurs peuvent ajouter, mettre à jour et supprimer des ingrédients utilisés dans les produits.

3. Affichage des produits avec leurs ingrédients : J'ai développé une fonctionnalité qui permet d'afficher une liste de produits avec les ingrédients associés. Les utilisateurs peuvent voir les détails des produits et les ingrédients qui les composent.

4. Gestion du stock : J'ai ajouté une fonctionnalité de gestion du stock pour les produits et les ingrédients. Les utilisateurs peuvent voir le niveau de stock actuel de chaque produit ou ingrédient. De plus, un avertissement est affiché lorsque le stock atteint un seuil critique, indiquant aux utilisateurs de passer une commande pour éviter la rupture de stock.

## Ce qui manque

Bien que j'ai réussi à mettre en œuvre les principales fonctionnalités du projet, il y a encore quelques points qui pourraient être améliorés ou ajoutés :

1. Authentification et gestion des utilisateurs : Actuellement, l'application ne dispose pas d'un système d'authentification pour gérer les utilisateurs. Il serait bénéfique de mettre en place un système d'authentification pour sécuriser l'accès à l'application et permettre à différents utilisateurs d'avoir des autorisations différentes.

2. Validation des formulaires : Il serait utile d'ajouter une validation des formulaires pour s'assurer que les utilisateurs saisissent des données correctes et cohérentes lors de la création ou de la modification de produits et d'ingrédients.

3. Recherche et filtrage : Une fonctionnalité de recherche et de filtrage des produits pourrait être ajoutée pour permettre aux utilisateurs de trouver rapidement des produits spécifiques en fonction de certains critères tels que le nom, la description ou les ingrédients.

4. Script de remplissage de la base de données : Pour rendre l'application plus conviviale et faciliter sa découverte, il serait intéressant d'ajouter un script permettant de remplir la base de données avec des données initiales. Je regrette de pas avoir pu vous faire découvrir l'environement Food de mon pays la République démocratique du Congo

## Idées de fonctionnalités intéressantes

Voici quelques idées de fonctionnalités intéressantes qui pourraient être ajoutées à l'application :

1. Gestion des commandes : Ajouter la possibilité pour les utilisateurs de passer des commandes de produits et d'ingrédients

 directement depuis l'application. Cela pourrait inclure la création de paniers d'achat, le suivi des commandes et l'intégration de passerelles de paiement.

2. Génération de rapports : Permettre aux utilisateurs de générer des rapports sur les ventes, les stocks, les commandes, etc. Ces rapports pourraient fournir des informations précieuses pour la gestion et la prise de décision.

3. Intégration de recettes : Permettre aux utilisateurs de créer et de partager des recettes qui utilisent certains produits et ingrédients. Les utilisateurs pourraient ajouter des instructions, des photos et des notes aux recettes.

4. Système de recommandation : Mettre en place un système de recommandation de produits ou d'ingrédients basé sur les préférences et les habitudes d'achat des utilisateurs. Cela pourrait aider les utilisateurs à découvrir de nouveaux produits et à améliorer leur expérience globale.

5. Internationalisation : Ajouter la possibilité de traduire l'application dans différentes langues pour atteindre un public plus large et faciliter son utilisation pour les utilisateurs non anglophones.

Ces idées de fonctionnalités pourraient être explorées pour améliorer et étendre l'application de gestion des produits et ingrédients.

Compound Components au niveau du front-end

Dans notre application de gestion des produits et ingrédients, j'ai adopté une approche de conception appelée "Compound Components" pour permettre une meilleure organisation et modularité de nos composants.
Qu'est-ce que Compound Components ?

Compound Components est un modèle de conception qui permet de regrouper plusieurs composants étroitement liés dans un composant parent, créant ainsi une relation de composition entre eux. Cela permet de structurer et de contrôler les interactions entre les composants, tout en offrant une interface simple et intuitive pour les utilisateurs.

Comment ai-je mis en œuvre Compound Components ?

J'ai identifié des fonctionnalités connexes qui nécessitaient une collaboration étroite entre plusieurs composants, et je les ai regroupées en un composant parent qui agit comme un conteneur pour ces composants connexes. Voici les étapes générales que j'ai suivies :

    Identification des fonctionnalités : J'ai identifié les fonctionnalités de notre application qui nécessitaient une interaction entre plusieurs composants. Dans notre cas, il s'agissait notamment de la création, de la modification et de la suppression de produits et d'ingrédients.

    Création du composant parent : J'ai créé un composant parent qui agit comme un conteneur pour les composants connexes. Ce composant parent gère l'état et les données partagées, et expose des API pour interagir avec les composants enfants.

    Définition des composants enfants : J'ai défini les composants enfants qui sont inclus dans le composant parent. Chaque composant enfant est responsable d'une partie spécifique de la fonctionnalité globale. Par exemple, j'ai créé des composants enfants pour le formulaire de création de produits, le formulaire de création d'ingrédients, l'affichage des produits, etc.

    Communication entre les composants : J'ai établi des mécanismes de communication entre les composants enfants et le composant parent. Cela peut être réalisé en passant des props, en utilisant des contextes ou en utilisant des méthodes de rappel. Par exemple, lorsque le formulaire de création d'un produit est soumis, il communique avec le composant parent pour ajouter le nouveau produit à la liste des produits.

    Utilisation du composant parent : Dans d'autres parties de notre application, j'ai utilisé le composant parent pour encapsuler les fonctionnalités liées. Cela me permet de réutiliser facilement ces fonctionnalités dans différentes parties de l'application.
    
Produits
- pouvoir consulter la liste des produits => ok
- pouvoir consulter un produit (avec ses propriétés et ingrédients) => ok
- pouvoir créer un produit => ok
- pouvoir éditer un produit => ok
- pouvoir supprimer un produit => ok
Ingrédients
- pouvoir consulter la liste des ingrédients => ok
- pouvoir consulter un ingrédient => ok
- pouvoir créer un ingrédient => ok
- pouvoir éditer un ingrédient => ok
- pouvoir supprimer un ingrédient => ok

Liaison Produits <> Ingrédients
- pouvoir lier des ingrédients à un produit => ok 

Fonctionnalités optionnelles
- utilisation des migrations TypeORM => j'ai essayé, vous pouvez toujours voir juste le code
- pagination => ok
- support du texte enrichi sur la description du produit (gras, italique, etc.) => ok
- hébergement d’image sur la fiche produit => manque de temps
- rédaction de tests => j'ai ecrit les tests mais pas assez de temps pour regler les prolèmes de config désolé ..

Je vous remercie pour ce projet très interessant, ca m'a beaucoup plus et je me suis amusé. Je vais maintenant travailler sur mes prochains projets avec graphQL, merci de me l'avoir fait découvrir ! 