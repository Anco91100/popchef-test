module.exports = [
  {
    name: "development",
    type: "sqlite",
    database: "db/database.sqlite",
    synchronize: true,
    logging: true,
    entities: ["src/entity/**/*.ts"],
    migrations: ["src/migration/**/*.ts"],
    subscribers: ["src/subscriber/**/*.ts"],
    cli: {
      entitiesDir: "src/entity",
      migrationsDir: "src/migration",
      subscribersDir: "src/subscriber",
    },
    driver: require("sqlite3"),
  },
  {
    name: "test",
    type: "sqlite",
    database: "db/test_db.sqlite",
    synchronize: true,
    logging: false,
    entities: ["src/entity/**/*.ts"],
    cli: {
      entitiesDir: "src/entity",
    },
    driver: require("sqlite3"),
  },
];