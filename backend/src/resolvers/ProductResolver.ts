import { Product } from "../entity/Product";
import { Arg, Mutation, Query, Resolver } from "type-graphql";
import { ProductInput } from "../input/ProductInput";
import { ProductWithIngredientsInput } from "../input/ProductWithIngredients";
import { Ingredient } from "../entity/Ingredient";
import { ProductWithIngredients } from "../entity/ProductWithIngredients";
import { LessThan } from "typeorm";

@Resolver(() => Product)
export class ProductResolver {
  @Mutation(() => Product)
  async createProduct(
    @Arg("product", () => ProductInput) product: ProductInput
  ) {
    return Product.create({ ...product }).save();
  }

  @Mutation(() => Product)
  async createProductWithIngredients(
    @Arg("data") data: ProductWithIngredientsInput
  ): Promise<Product> {
    const product = new Product();
    product.name = data.name;
    product.description = data.description;
    product.stock = data.stock;

    const ingredients = await Ingredient.findByIds(data.ingredients);
    product.ingredients = ingredients;

    return product.save();
  }


  @Mutation(() => Product)
  async updateProduct(
    @Arg("id") id: number,
    @Arg("input", () => ProductInput) input: ProductInput
  ) {
    await Product.update({ id }, input);

    return Product.findOne({ where: { id } });
  }

  @Mutation(() => Product)
  async updateProductWithIngredients(
    @Arg("id") id: number,
    @Arg("data") data: ProductWithIngredientsInput
  ): Promise<Product | undefined> {
    const product = await Product.findOne({ where: { id } });
    if (!product) {
      throw new Error(`Product with id ${id} not found.`);
    }

    product.name = data.name;
    product.description = data.description;
    product.stock = data.stock;
    const ingredients = await Ingredient.findByIds(data.ingredients);
    product.ingredients = ingredients;

    return product.save();
  }

  @Mutation(() => Boolean)
  async deleteProduct(@Arg("id") id: number) {
    await Product.delete({ id });

    return true;
  }

  @Query(() => Product)
  async product(@Arg("id") id: number) {
    return Product.findOne({ where: { id } });
  }

  @Query(() => [ProductWithIngredients])
  async productsWithIngredients(): Promise<ProductWithIngredients[]> {
    const products = await Product.find({ relations: ["ingredients"] });

    return products.map((product) => {
      const productWithIngredients = new ProductWithIngredients();
      productWithIngredients.id = product.id;
      productWithIngredients.name = product.name;
      productWithIngredients.description = product.description;
      productWithIngredients.ingredients = product.ingredients;
      productWithIngredients.stock = product.stock;

      return productWithIngredients;
    });
  }

  @Query(() => [Product])
  async products() {
    return Product.find();
  }

  @Query(() => [Product])
  async lowStockProducts(
  ): Promise<Product[]> {
    const products = await Product.find({ where: { stock: LessThan(50) } });
    return products;
  }
}
