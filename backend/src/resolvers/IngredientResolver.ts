import { Arg, Mutation, Query, Resolver } from "type-graphql";
import { Ingredient } from "../entity/Ingredient";
import { IngredientInput } from "../input/IngredientInput";
import { LessThan } from "typeorm";

@Resolver(() => Ingredient)
export class IngredientResolver {
  @Mutation(() => Ingredient)
  async createIngredient(
    @Arg("Ingredient", () => IngredientInput) ingredient: IngredientInput
  ) {
    return Ingredient.create({ ...ingredient }).save();
  }

  @Mutation(() => Ingredient)
  async updateIngredient(
    @Arg("id") id: number,
    @Arg("input", () => IngredientInput) input: IngredientInput
  ) {
    await Ingredient.update({ id }, input);
    return Ingredient.findOne({ where: { id } });
  }

  @Mutation(() => Boolean)
  async deleteIngredient(@Arg("id") id: number) {
    await Ingredient.delete({ id });

    return true;
  }

  @Query(() => Ingredient)
  async ingredient(@Arg("id") id: number) {
    return Ingredient.findOne({ where: { id } });
  }

  @Query(() => [Ingredient])
  async ingredients() {
    return Ingredient.find();
  }

  @Query(() => [Ingredient])
  async lowStockIngredients(
  ): Promise<Ingredient[]> {
    const ingredients = await Ingredient.find({ where: { stock: LessThan(50) } });
    return ingredients;
  }
}
