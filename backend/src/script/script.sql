-- Insertion des ingrédients
INSERT INTO ingredient (name, stock) VALUES
  ('Poisson', 100),
  ('Manioc', 200),
  ('Huile de palme', 150),
  ('Piment', 300),
  ('Oignon', 250),
  ('Tomate', 200),
  ('Pâte d''arachide', 180),
  ('Haricots', 45),
  ('Banane plantain', 250),
  ('Aubergine', 150),
  ('Viande de bœuf', 300),
  ('Viande de chèvre', 200),
  ('Viande de porc', 180),
  ('Piment en poudre', 48),
  ('Feuilles de manioc', 250);

-- Insertion des produits
INSERT INTO product (name, description, stock) VALUES
  ('Fufu', 'Le fufu est un plat traditionnel congolais à base de manioc. Il est souvent consommé avec une sauce de poisson.', 30),
  ('Moyindo', 'Le moyindo est un plat congolais à base de banane plantain et de haricots. C''est un plat très nourrissant.', 80),
  ('Madesu', 'Le madesu est un plat congolais à base de viande de bœuf et d''oignon. Il est généralement servi avec du riz.', 60),
  ('Liboke', 'Le liboke est un plat congolais à base de poisson et de pâte d''arachide. Il est cuit dans des feuilles de bananier.', 70),
  ('Chikwangue', 'Le chikwangue est une spécialité congolaise à base de manioc fermenté. Il est souvent accompagné de viande.', 90),
  ('Saka-saka', 'Le saka-saka est un plat congolais à base de feuilles de manioc et de viande. Il est riche en saveurs.', 40),
  ('Ngulu', 'Le ngulu est un plat congolais à base de poisson fumé et de piment. Il est souvent accompagné de manioc.', 75),
  ('Maboke', 'Le maboke est un plat congolais à base de viande de porc et de feuilles de manioc. Il est cuit dans des feuilles de bananier.', 55),
  ('Mbisi', 'Le mbisi est un plat congolais à base de poisson et de pâte de tomate. Il est accompagné de manioc.', 65),
  ('Biteku-teku', 'Le biteku-teku est un plat congolais à base de viande de chèvre et de piment. Il est épicé et savoureux.', 45),
  ('Makayabu', 'Le makayabu est un plat congolais à base de poisson séché et de piment. Il est souvent consommé avec du manioc.', 70),
  ('Soso', 'Le soso est un plat congolais à base de viande de bœuf et de légumes. Il est généralement servi avec du riz.', 60),
  ('Mwamba', 'Le mwamba est un plat congolais à base de poulet et de légumes. Il est riche en saveurs et en épices.', 50),
  ('Yassa', 'Le yassa est un plat congolais à base de viande de poulet et d''oignon. Il est souvent accompagné de riz.', 75),
  ('Mafé', 'Le mafé est un plat congolais à base de viande et de pâte d''arachide. Il est servi avec du riz ou du foufou.', 55),
  ('Mpondu', 'Le mpondu est un plat congolais à base de feuilles de manioc et de légumes. Il est généralement accompagné de poisson.', 85);

-- Associations des ingrédients aux produits
INSERT INTO product_ingredients_ingredient (productId, ingredientId) VALUES
  -- Fufu
  ((SELECT id FROM product WHERE name = 'Fufu'), (SELECT id FROM ingredient WHERE name = 'Manioc')),
  ((SELECT id FROM product WHERE name = 'Fufu'), (SELECT id FROM ingredient WHERE name = 'Poisson')),
  -- Moyindo
  ((SELECT id FROM product WHERE name = 'Moyindo'), (SELECT id FROM ingredient WHERE name = 'Banane plantain')),
  ((SELECT id FROM product WHERE name = 'Moyindo'), (SELECT id FROM ingredient WHERE name = 'Haricots')),
  -- Madesu
  ((SELECT id FROM product WHERE name = 'Madesu'), (SELECT id FROM ingredient WHERE name = 'Viande de bœuf')),
  ((SELECT id FROM product WHERE name = 'Madesu'), (SELECT id FROM ingredient WHERE name = 'Oignon')),
  -- Liboke
  ((SELECT id FROM product WHERE name = 'Liboke'), (SELECT id FROM ingredient WHERE name = 'Poisson')),
  ((SELECT id FROM product WHERE name = 'Liboke'), (SELECT id FROM ingredient WHERE name = 'Pâte d''arachide')),
  -- Chikwangue
  ((SELECT id FROM product WHERE name = 'Chikwangue'), (SELECT id FROM ingredient WHERE name = 'Manioc')),
  -- Saka-saka
  ((SELECT id FROM product WHERE name = 'Saka-saka'), (SELECT id FROM ingredient WHERE name = 'Feuilles de manioc')),
  ((SELECT id FROM product WHERE name = 'Saka-saka'), (SELECT id FROM ingredient WHERE name = 'Viande de bœuf')),
  -- Ngulu
  ((SELECT id FROM product WHERE name = 'Ngulu'), (SELECT id FROM ingredient WHERE name = 'Poisson')),
  ((SELECT id FROM product WHERE name = 'Ngulu'), (SELECT id FROM ingredient WHERE name = 'Piment')),
  -- Maboke
  ((SELECT id FROM product WHERE name = 'Maboke'), (SELECT id FROM ingredient WHERE name = 'Viande de porc')),
  ((SELECT id FROM product WHERE name = 'Maboke'), (SELECT id FROM ingredient WHERE name = 'Feuilles de manioc')),
  -- Mbisi
  ((SELECT id FROM product WHERE name = 'Mbisi'), (SELECT id FROM ingredient WHERE name = 'Poisson')),
  ((SELECT id FROM product WHERE name = 'Mbisi'), (SELECT id FROM ingredient WHERE name = 'Pâte de tomate')),
  -- Biteku-teku
  ((SELECT id FROM product WHERE name = 'Biteku-teku'), (SELECT id FROM ingredient WHERE name = 'Viande de chèvre')),
  ((SELECT id FROM product WHERE name = 'Biteku-teku'), (SELECT id FROM ingredient WHERE name = 'Piment')),
  -- Makayabu
  ((SELECT id FROM product WHERE name = 'Makayabu'), (SELECT id FROM ingredient WHERE name = 'Poisson séché')),
  ((SELECT id FROM product WHERE name = 'Makayabu'), (SELECT id FROM ingredient WHERE name = 'Piment')),
  -- Soso
  ((SELECT id FROM product WHERE name = 'Soso'), (SELECT id FROM ingredient WHERE name = 'Viande de bœuf')),
  ((SELECT id FROM product WHERE name = 'Soso'), (SELECT id FROM ingredient WHERE name = 'Légumes')),
  -- Mwamba
  ((SELECT id FROM product WHERE name = 'Mwamba'), (SELECT id FROM ingredient WHERE name = 'Poulet')),
  ((SELECT id FROM product WHERE name = 'Mwamba'), (SELECT id FROM ingredient WHERE name = 'Légumes')),
  -- Yassa
  ((SELECT id FROM product WHERE name = 'Yassa'), (SELECT id FROM ingredient WHERE name = 'Viande de poulet')),
  ((SELECT id FROM product WHERE name = 'Yassa'), (SELECT id FROM ingredient WHERE name = 'Oignon')),
  -- Mafé
  ((SELECT id FROM product WHERE name = 'Mafé'), (SELECT id FROM ingredient WHERE name = 'Viande')),
  ((SELECT id FROM product WHERE name = 'Mafé'), (SELECT id FROM ingredient WHERE name = 'Pâte d''arachide')),
  -- Mpondu
  ((SELECT id FROM product WHERE name = 'Mpondu'), (SELECT id FROM ingredient WHERE name = 'Feuilles de manioc')),
  ((SELECT id FROM product WHERE name = 'Mpondu'), (SELECT id FROM ingredient WHERE name = 'Légumes'));
