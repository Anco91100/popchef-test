import { Field, ObjectType } from "type-graphql";
import { Ingredient } from "./Ingredient";
import { Product } from "./Product";

@ObjectType()
export class ProductWithIngredients extends Product {
  @Field(() => [Ingredient])
  ingredients: Ingredient[];
}
