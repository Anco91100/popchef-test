import { Field, ObjectType, Int } from "type-graphql";
import { Entity, PrimaryGeneratedColumn, Column, BaseEntity, JoinTable, ManyToMany } from "typeorm";
import { Ingredient } from "./Ingredient";

@ObjectType()
@Entity()
export class Product extends BaseEntity {
  @Field()
  @PrimaryGeneratedColumn()
  id: number;

  @Field()
  @Column()
  name: string;

  @Field()
  @Column()
  description: string;

  @Field(() => [Ingredient])
  @ManyToMany(() => Ingredient, ingredient => ingredient.products, { onDelete: "CASCADE" })
  @JoinTable()
  ingredients: Ingredient[];

  @Field(() => Int)
  @Column()
  stock: number;
}
