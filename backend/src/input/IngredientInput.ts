import { Field, InputType } from "type-graphql";

@InputType()
export class IngredientInput {
  @Field({ nullable: true })
  id: number;

  @Field({ nullable: true })
  name: string;

  @Field({ nullable: true })
  stock: number;
}
