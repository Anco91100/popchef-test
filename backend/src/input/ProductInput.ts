import { Field, InputType } from "type-graphql";

@InputType()
export class ProductInput {
  @Field({ nullable: true })
  id: number;

  @Field({ nullable: true })
  name: string;

  @Field({ nullable: true })
  description: string;

  @Field({ nullable: true })
  stock: number;
}
