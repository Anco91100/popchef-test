import { Field, ID, InputType } from "type-graphql";

@InputType()
export class ProductWithIngredientsInput {
  @Field()
  name: string;

  @Field()
  description: string;

  @Field(() => [ID])
  ingredients: number[];

  @Field({ nullable: true })
  stock: number;
}

