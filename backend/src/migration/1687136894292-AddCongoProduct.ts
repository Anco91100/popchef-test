import { MigrationInterface, QueryRunner } from "typeorm"

export class AddCongoProduct1687136894292 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
          -- Insertion des ingrédients
          INSERT INTO ingredients (name, stock) VALUES
            ('Poisson', 100),
            ('Manioc', 200),
            ('Huile de palme', 150),
            ('Piment', 300),
            ('Oignon', 250),
            ('Tomate', 200),
            ('Pâte d'arachide', 180),
            ('Haricots', 45),
            ('Banane plantain', 250),
            ('Aubergine', 150),
            ('Viande de bœuf', 300),
            ('Viande de chèvre', 200),
            ('Viande de porc', 180),
            ('Piment en poudre', 48),
            ('Feuilles de manioc', 250);
    
          -- Insertion des produits
          INSERT INTO products (name, description, stock) VALUES
            ('Fufu', 'Le fufu est un plat traditionnel congolais à base de manioc. Il est souvent consommé avec une sauce de poisson.', 30),
            ('Moyindo', 'Le moyindo est un plat congolais à base de banane plantain et de haricots. C'est un plat très nourrissant.', 80),
            ('Madesu', 'Le madesu est un plat congolais à base de viande de bœuf et d'oignon. Il est généralement servi avec du riz.', 60),
            ('Liboke', 'Le liboke est un plat congolais à base de poisson et de pâte d'arachide. Il est cuit dans des feuilles de bananier.', 70),
            ('Chikwangue', 'Le chikwangue est une spécialité congolaise à base de manioc fermenté. Il est souvent accompagné de viande.', 90),
            ('Saka-saka', 'Le saka-saka est un plat congolais à base de feuilles de manioc et de viande. Il est riche en saveurs.', 40),
            ('Ngulu', 'Le ngulu est un plat congolais à base de poisson fumé et de piment. Il est souvent accompagné de manioc.', 75),
            ('Maboke', 'Le maboke est un plat congolais à base de viande de porc et de feuilles de manioc. Il est cuit dans des feuilles de bananier.', 55),
            ('Mbisi', 'Le mbisi est un plat congolais à base de poisson et de pâte de tomate. Il est accompagné de manioc.', 65),
            ('Biteku-teku', 'Le biteku-teku est un plat congolais à base de viande de chèvre et de piment. Il est épicé et savoureux.', 45),
            ('Makayabu', 'Le makayabu est un plat congolais à base de poisson séché et de piment. Il est souvent consommé avec du manioc.', 70),
            ('Soso', 'Le soso est un plat congolais à base de viande de bœuf et de légumes. Il est généralement servi avec du riz.', 60),
            ('Mwamba', 'Le mwamba est un plat congolais à base de poulet et de légumes. Il est riche en saveurs et en épices.', 50),
            ('Yassa', 'Le yassa est un plat congolais à base de viande de poulet et d'oignon. Il est souvent accompagné de riz.', 75),
            ('Mafé', 'Le mafé est un plat congolais à base de viande et de pâte d'arachide. Il est servi avec du riz ou du foufou.', 55),
            ('Mpondu', 'Le mpondu est un plat congolais à base de feuilles de manioc et de légumes. Il est généralement accompagné de poisson.', 85);
    
          -- Associations des ingrédients aux produits
          INSERT INTO product_ingredients (productId, ingredientId) VALUES
            -- Fufu
            ((SELECT id FROM products WHERE name = 'Fufu'), (SELECT id FROM ingredients WHERE name = 'Manioc')),
            ((SELECT id FROM products WHERE name = 'Fufu'), (SELECT id FROM ingredients WHERE name = 'Poisson')),
            -- Moyindo
            ((SELECT id FROM products WHERE name = 'Moyindo'), (SELECT id FROM ingredients WHERE name = 'Banane plantain')),
            ((SELECT id FROM products WHERE name = 'Moyindo'), (SELECT id FROM ingredients WHERE name = 'Haricots')),
            -- Madesu
            ((SELECT id FROM products WHERE name = 'Madesu'), (SELECT id FROM ingredients WHERE name = 'Viande de bœuf')),
            ((SELECT id FROM products WHERE name = 'Madesu'), (SELECT id FROM ingredients WHERE name = 'Oignon')),
            -- Liboke
            ((SELECT id FROM products WHERE name = 'Liboke'), (SELECT id FROM ingredients WHERE name = 'Poisson')),
            ((SELECT id FROM products WHERE name = 'Liboke'), (SELECT id FROM ingredients WHERE name = 'Pâte d'arachide')),
            -- Chikwangue
            ((SELECT id FROM products WHERE name = 'Chikwangue'), (SELECT id FROM ingredients WHERE name = 'Manioc')),
            -- Saka-saka
            ((SELECT id FROM products WHERE name = 'Saka-saka'), (SELECT id FROM ingredients WHERE name = 'Feuilles de manioc')),
            ((SELECT id FROM products WHERE name = 'Saka-saka'), (SELECT id FROM ingredients WHERE name = 'Viande de bœuf')),
            -- Ngulu
            ((SELECT id FROM products WHERE name = 'Ngulu'), (SELECT id FROM ingredients WHERE name = 'Poisson')),
            ((SELECT id FROM products WHERE name = 'Ngulu'), (SELECT id FROM ingredients WHERE name = 'Piment')),
            -- Maboke
            ((SELECT id FROM products WHERE name = 'Maboke'), (SELECT id FROM ingredients WHERE name = 'Viande de porc')),
            ((SELECT id FROM products WHERE name = 'Maboke'), (SELECT id FROM ingredients WHERE name = 'Feuilles de manioc')),
            -- Mbisi
            ((SELECT id FROM products WHERE name = 'Mbisi'), (SELECT id FROM ingredients WHERE name = 'Poisson')),
            ((SELECT id FROM products WHERE name = 'Mbisi'), (SELECT id FROM ingredients WHERE name = 'Pâte de tomate')),
            -- Biteku-teku
            ((SELECT id FROM products WHERE name = 'Biteku-teku'), (SELECT id FROM ingredients WHERE name = 'Viande de chèvre')),
            ((SELECT id FROM products WHERE name = 'Biteku-teku'), (SELECT id FROM ingredients WHERE name = 'Piment')),
            -- Makayabu
            ((SELECT id FROM products WHERE name = 'Makayabu'), (SELECT id FROM ingredients WHERE name = 'Poisson séché')),
            ((SELECT id FROM products WHERE name = 'Makayabu'), (SELECT id FROM ingredients WHERE name = 'Piment')),
            -- Soso
            ((SELECT id FROM products WHERE name = 'Soso'), (SELECT id FROM ingredients WHERE name = 'Viande de bœuf')),
            ((SELECT id FROM products WHERE name = 'Soso'), (SELECT id FROM ingredients WHERE name = 'Légumes')),
            -- Mwamba
            ((SELECT id FROM products WHERE name = 'Mwamba'), (SELECT id FROM ingredients WHERE name = 'Poulet')),
            ((SELECT id FROM products WHERE name = 'Mwamba'), (SELECT id FROM ingredients WHERE name = 'Légumes')),
            -- Yassa
            ((SELECT id FROM products WHERE name = 'Yassa'), (SELECT id FROM ingredients WHERE name = 'Viande de poulet')),
            ((SELECT id FROM products WHERE name = 'Yassa'), (SELECT id FROM ingredients WHERE name = 'Oignon')),
            -- Mafé
            ((SELECT id FROM products WHERE name = 'Mafé'), (SELECT id FROM ingredients WHERE name = 'Viande')),
            ((SELECT id FROM products WHERE name = 'Mafé'), (SELECT id FROM ingredients WHERE name = 'Pâte d'arachide')),
            -- Mpondu
            ((SELECT id FROM products WHERE name = 'Mpondu'), (SELECT id FROM ingredients WHERE name = 'Feuilles de manioc')),
            ((SELECT id FROM products WHERE name = 'Mpondu'), (SELECT id FROM ingredients WHERE name = 'Légumes'));
        `);
      }

      public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
          -- Suppression des associations d'ingrédients aux produits
          DELETE FROM product_ingredients WHERE productId IN (
            (SELECT id FROM products WHERE name IN (
              'Fufu',
              'Moyindo',
              'Madesu',
              'Liboke',
              'Chikwangue',
              'Saka-saka',
              'Ngulu',
              'Maboke',
              'Mbisi',
              'Biteku-teku',
              'Makayabu',
              'Soso',
              'Mwamba',
              'Yassa',
              'Mafé',
              'Mpondu'
            ))
          );
    
          -- Suppression des produits
          DELETE FROM products WHERE name IN (
            'Fufu',
            'Moyindo',
            'Madesu',
            'Liboke',
            'Chikwangue',
            'Saka-saka',
            'Ngulu',
            'Maboke',
            'Mbisi',
            'Biteku-teku',
            'Makayabu',
            'Soso',
            'Mwamba',
            'Yassa',
            'Mafé',
            'Mpondu'
          );
    
          -- Suppression des ingrédients
          DELETE FROM ingredients WHERE name IN (
            'Poisson',
            'Manioc',
            'Huile de palme',
            'Piment',
            'Oignon',
            'Tomate',
            'Pâte d'arachide',
            'Haricots',
            'Banane plantain',
            'Aubergine',
            'Viande de bœuf',
            'Viande de chèvre',
            'Viande de porc',
            'Piment en poudre',
            'Feuilles de manioc'
          );
        `);
      }

}
