import { MigrationInterface, QueryRunner } from "typeorm"

export class AddIngredients1687136932525 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
          INSERT INTO ingredients (name, stock) VALUES
            ('Curry', 250),
            ('Sel', 500),
            ('Poivre', 400),
            ('Paprika', 150),
            ('Cumin', 200),
            ('Persil', 100),
            ('Thym', 80),
            ('Romarin', 90),
            ('Origan', 120),
            ('Basilic', 70),
            ('Sauce soja', 300),
            ('Sauce tomate', 400),
            ('Moutarde', 250),
            ('Mayonnaise', 200),
            ('Vinaigre balsamique', 150),
            ('Vinaigre de vin', 180),
            ('Huile d'olive', 300),
            ('Huile de tournesol', 250),
            ('Huile de sésame', 200),
            ('Huile de coco', 150),
            ('Ail', 400),
            ('Oignon', 350),
            ('Échalote', 200),
            ('Gingembre', 250),
            ('Citron', 300),
            ('Lait de coco', 150),
            ('Crème fraîche', 200),
            ('Fromage râpé', 250),
            ('Parmesan', 200),
            ('Pain', 300),
            ('Pâtes', 400),
            ('Riz', 500);
        `);
      }

      public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
          DELETE FROM ingredients WHERE name IN (
            'Curry',
            'Sel',
            'Poivre',
            'Paprika',
            'Cumin',
            'Persil',
            'Thym',
            'Romarin',
            'Origan',
            'Basilic',
            'Sauce soja',
            'Sauce tomate',
            'Moutarde',
            'Mayonnaise',
            'Vinaigre balsamique',
            'Vinaigre de vin',
            'Huile d'olive',
            'Huile de tournesol',
            'Huile de sésame',
            'Huile de coco',
            'Ail',
            'Oignon',
            'Échalote',
            'Gingembre',
            'Citron',
            'Lait de coco',
            'Crème fraîche',
            'Fromage râpé',
            'Parmesan',
            'Pain',
            'Pâtes',
            'Riz'
          );
        `);
      }

}
