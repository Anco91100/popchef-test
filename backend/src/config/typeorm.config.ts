import { DataSource } from 'typeorm';
const dataSource = new DataSource(  {
    name: "development",
    type: "sqlite",
    database: "db/database.sqlite",
    synchronize: true,
    logging: true,
    entities: ["src/entity/**/*.ts"],
    migrations: ["src/migration/**/*.ts"],
    subscribers: ["src/subscriber/**/*.ts"],
    driver: require("sqlite3"),
  }
  );

dataSource.initialize().then(async (n)=>{
    console.log(n);
})

export default dataSource;
