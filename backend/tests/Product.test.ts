import { createTestContext } from "./__helpers";

const ctx = createTestContext();


describe("Product test", () => {
  test("test createProduct mutation", async () => {
    const productData = {
      name: "Pizza Margherita",
      description: "Delicious pizza",
      stock: 10
    };

    let result = await ctx.client.request(`
      mutation CreateProduct($product: ProductInput!) {
        createProduct(product: $product) {
          name
          description
          stock
        }
      }
    `, { product: productData });

    expect(result).toMatchInlineSnapshot(`
      Object {
        "createProduct": Object {
          "name": "Pizza Margherita",
          "description": "Delicious pizza"
          "stock": 10,
        },
      }
    `);
  });

  test("test updateProduct mutation", async () => {
    const productData = {
      name: "Pizza Margherita",
      description: "Delicious pizza",
      stock: 10
    };

    let createProductResult: any = await ctx.client.request(`
      mutation CreateProduct($product: ProductInput!) {
        createProduct(product: $product) {
          id
          name
          description
          stock
        }
      }
    `, { product: productData });

    const updateData = {
      name: "Pizza Margherita Deluxe",
      description: "Delicious and upgraded pizza",
      stock: 15
    };

    let result = await ctx.client.request(`
      mutation UpdateProduct($input: ProductInput!, $updateProductId: Float!) {
        updateProduct(input: $input, id: $updateProductId) {
          description
          stock
          name
        }
      }
    `, {
      input: updateData,
      updateProductId: createProductResult.createProduct.id
    });

    expect(result).toMatchInlineSnapshot(`
      Object {
        "updateProduct": Object {
          "description": "Delicious and upgraded pizza",
          "stock": 15,
          "name": "Pizza Margherita Deluxe",
        },
      }
    `);
  });

  test("test deleteProduct mutation", async () => {
    const productData = {
      name: "Pizza Margherita",
      description: "Delicious pizza",
      stock: 10
    };

    let createProductResult: any = await ctx.client.request(`
      mutation CreateProduct($product: ProductInput!) {
        createProduct(product: $product) {
          id
          name
          description
          stock
        }
      }
    `, { product: productData });

    let result = await ctx.client.request(`
      mutation DeleteProduct($deleteProductId: Float!) {
        deleteProduct(id: $deleteProductId)
      }
    `, {
      deleteProductId: createProductResult.createProduct.id
    });

    expect(result).toMatchInlineSnapshot(`
      Object {
        "deleteProduct": true,
      }
    `);
  });

})

describe("ProductWithIngredients test", () => {

  test("test deleteProductWithIngredients", async () => {

    const ingredientData = {
      name: "Tomato",
      stock: 10
    };

    let createIngredientResult: any = await ctx.client.request(`
      mutation CreateIngredient($ingredient: IngredientInput!) {
        createIngredient(ingredient: $ingredient) {
          id
          name
          stock
        }
      }
    `, { ingredient: ingredientData });

    const createData = {
      name: "Pizza Margherita",
      description: "Delicious pizza",
      stock: 150,
      ingredients: [createIngredientResult.createIngredient.id]
    };

    let createResult: any = await ctx.client.request(`
      mutation CreateProductWithIngredients($data: ProductWithIngredientsInput!) {
        createProductWithIngredients(data: $data) {
          id
          description
          ingredients {
            name
            stock
          }
          name
        }
      }
    `, { data: createData });

    const productId = createResult.createProductWithIngredients.id;

    let deleteResult = await ctx.client.request(`
      mutation DeleteProduct($deleteProductId: Float!) {
        deleteProduct(id: $deleteProductId)
      }
    `, { deleteProductId: productId });

    expect(deleteResult).toEqual({ deleteProduct: true });

  })

  test("test CreateProductWithIngredients", async () => {
    const ingredientData = {
      name: "Tomato",
      stock: 10
    };

    let createIngredientResult: any = await ctx.client.request(`
      mutation CreateIngredient($ingredient: IngredientInput!) {
        createIngredient(ingredient: $ingredient) {
          id
          name
          stock
        }
      }
    `, { ingredient: ingredientData });

    const createData = {
      name: "Pizza Margherita",
      description: "Delicious pizza",
      stock: 150,
      ingredients: [createIngredientResult.createIngredient.id]
    };

    let createResult = await ctx.client.request(`
      mutation CreateProductWithIngredients($data: ProductWithIngredientsInput!) {
        createProductWithIngredients(data: $data) {
          id
          description
          ingredients {
            name
            stock
          }
          name
        }
      }
    `, { data: createData });

    expect(createResult).toMatchInlineSnapshot(`
      Object {
        "createProductWithIngredients": Object {
          "description": "Delicious pizza",
          "id": 1,
          "ingredients": Array [
            Object {
              "name": "Tomato",
              "stock": 10,
            }
          ],
          "name": "Pizza Margherita",
        },
      }
    `);
  });

  test("test UpdateProductWithIngredients", async () => {
    const ingredientData = {
      name: "Tomato",
      stock: 10
    };
  
    let createIngredientResult: any = await ctx.client.request(`
        mutation CreateIngredient($ingredient: IngredientInput!) {
          createIngredient(ingredient: $ingredient) {
            id
            name
            stock
          }
        }
      `, { ingredient: ingredientData });
  
    const createData = {
      name: "Pizza Margherita",
      description: "Delicious pizza",
      stock: 150,
      ingredients: [createIngredientResult.createIngredient.id]
    };
  
    let createResult: any = await ctx.client.request(`
        mutation CreateProductWithIngredients($data: ProductWithIngredientsInput!) {
          createProductWithIngredients(data: $data) {
            id
            description
            ingredients {
              name
            }
            name
            stock
          }
        }
      `, { data: createData });
  
    const productId = createResult.createProductWithIngredients.id;
  
    const updateData = {
      name: "Pizza Margherita Deluxe",
      description: "Delicious and upgraded pizza",
      ingredients: [createIngredientResult.createIngredient.id]
    };
  
    let updateResult = await ctx.client.request(`
        mutation UpdateProductWithIngredients($data: ProductWithIngredientsInput!, $updateProductWithIngredientsId: Float!) {
          updateProductWithIngredients(data: $data, id: $updateProductWithIngredientsId) {
            description
            ingredients {
              name
            }
            name
            stock
          }
        }
      `, {
      data: updateData,
      updateProductWithIngredientsId: productId
    });
  
    expect(updateResult).toMatchInlineSnapshot(`
        Object {
          "updateProductWithIngredients": Object {
            "description": "Delicious and upgraded pizza",
            "ingredients": Array [
              Object {
                "name": "Tomato",
              }
            ],
            "name": "Pizza Margherita Deluxe",
            "stock": 150,
          },
        }
      `);
  });

})





