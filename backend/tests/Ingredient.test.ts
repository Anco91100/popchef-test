import { createTestContext } from "./__helpers";

const ctx = createTestContext();

describe("Ingredient test", () => {
  test("test createIngredient", async () => {
    const ingredientData = {
      name: "Tomato",
      stock: 10
    };

    let result = await ctx.client.request(`
      mutation CreateIngredient($ingredient: IngredientInput!) {
        createIngredient(ingredient: $ingredient) {
          id
          name
          stock
        }
      }
    `, { ingredient: ingredientData });

    expect(result).toMatchInlineSnapshot(`
      Object {
        "createIngredient": Object {
          "id": 1,
          "name": "Tomato",
          "stock": 10
        },
      }
    `);
  });

  test("test deleteIngredient mutation", async () => {
    const ingredientData = {
        name: "Tomato",
        stock: 10
      };
  
      let createIngredientResult: any = await ctx.client.request(`
        mutation CreateIngredient($ingredient: IngredientInput!) {
          createIngredient(ingredient: $ingredient) {
            id
            name
            stock
          }
        }
      `, { ingredient: ingredientData });

    let result = await ctx.client.request(`
      mutation DeleteIngredient($deleteIngredientId: Float!) {
        deleteIngredient(id: $deleteIngredientId)
      }
    `, {
      deleteIngredientId: createIngredientResult.createIngredient.id
    });

    expect(result).toMatchInlineSnapshot(`
      Object {
        "deleteIngredient": true,
      }
    `);
  });

  test("test updateIngredient mutation", async () => {
    const ingredientData = {
        name: "Tomato",
        stock: 10
      };
  
      let createIngredientResult: any = await ctx.client.request(`
        mutation CreateIngredient($ingredient: IngredientInput!) {
          createIngredient(ingredient: $ingredient) {
            id
            name
            stock
          }
        }
      `, { ingredient: ingredientData });

    const updateData = {
      name: "New Tomato",
      stock: 15
    };

    let result = await ctx.client.request(`
      mutation UpdateIngredient($input: IngredientInput!, $updateIngredientId: Float!) {
        updateIngredient(input: $input, id: $updateIngredientId) {
          name
          stock
        }
      }
    `, {
      input: updateData,
      updateIngredientId: createIngredientResult.createIngredient.id
    });

    expect(result).toMatchInlineSnapshot(`
      Object {
        "updateIngredient": Object {
          "name": "New Tomato",
          "stock": 15,
        },
      }
    `);
  });

  test("test ingredient query", async () => {
    const ingredientData = {
        name: "Tomato",
        stock: 10
      };
  
      let createIngredientResult: any = await ctx.client.request(`
        mutation CreateIngredient($ingredient: IngredientInput!) {
          createIngredient(ingredient: $ingredient) {
            id
            name
            stock
          }
        }
      `, { ingredient: ingredientData });

    let result = await ctx.client.request(`
      query Ingredient($ingredientId: Float!) {
        ingredient(id: $ingredientId) {
          name
          stock
        }
      }
    `, { ingredientId: createIngredientResult.createIngredient.id });

    expect(result).toMatchInlineSnapshot(`
      Object {
        "ingredient": Object {
          "name": "Tomato",
          "stock": 10,
        },
      }
    `);
  });

  
});
