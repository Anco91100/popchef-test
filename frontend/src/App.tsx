import fetch from 'cross-fetch';
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import { ApolloClient, InMemoryCache, ApolloProvider, HttpLink } from "@apollo/client";
import HomePage from "./pages/HomePage";
import NotFoundPage from "./pages/NotFoundPage";
import UpdateProductPage from "./pages/UpdateProductPage";
import UpdateIngredientPage from "./pages/UpdateIngredientPage";
import Product from "./components/Products/Product";
import ProductPage from "./pages/ProductPage";
import IngredientPage from "./pages/IngredientPage";
import Ingredient from "./components/Ingredients/Ingredient";
import Command from "./pages/CommandPage";

export const apolloClient = new ApolloClient({
  cache: new InMemoryCache(),
  link: new HttpLink({ uri: "http://localhost:4000", fetch })
});

const App: React.FC = () => {
  return (
    <ApolloProvider client={apolloClient}>
      <Router>
        <Routes>
          <Route path="*" element={<NotFoundPage />} />
          <Route path="/" element={<HomePage />} />
          <Route path="/update/product/:productId" element={<UpdateProductPage />} />
          <Route path="/update/ingredient/:ingredientId" element={<UpdateIngredientPage />} />
          <Route path="/product/:productId" element={<Product />} />
          <Route path="/ingredient/:ingredientId" element={<Ingredient />} />
          <Route path="/product" element={<ProductPage />} />
          <Route path="/ingredient" element={<IngredientPage />} />
          <Route path="/command" element={<Command />} />
        </Routes>
      </Router>
    </ApolloProvider>
  );
};

export default App;
