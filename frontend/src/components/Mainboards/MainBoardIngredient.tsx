import CreateIngredientPage from "../../pages/CreateIngredientPage";
import IngredientTable from "../Ingredients/IngredientTable";


const MainBoardIngredient: React.FC = () => {
    return (
        <div className=".main-wrap flex flex-col items-start p-[12px_0px_0px] w-full h-full">
            <div className="flex flex-col items-start p-[32px_0px_48px] gap-8 h-full w-full rounded-[40px_0px_0px_40px] bg-white">
                <div className=".header-section flex flex-col items-start gap-6 w-full h-[66px]">
                    <div className=".container flex flex-col items-start p-[0px_32px] gap-6 h-full w-full">
                        <div className=".content flex flex-row items-start gap-4 h-full w-full">
                            <h2 className="w-[875px] h-10 text-2xl text-[#101828 font-mdeium]" >Gérer vos ingredients </h2>
                        </div>
                    </div>
                </div>
                <div className="w-full h-full flex flex-row gap-5 justify-around p-4">
                    <div className="flex flex-col w-full h-full" >
                        <CreateIngredientPage />
                    </div>
                    <div className="flex flex-col w-full h-full" >
                        <IngredientTable />
                    </div>
                </div>

            </div>
        </div>
    );
};

export default MainBoardIngredient;
