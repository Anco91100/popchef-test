import Ingredient from './IngredientType';

export default interface Product {
    id: number;
    name: string;
    description: string;
    ingredients: Ingredient[];
    stock: number;
  }