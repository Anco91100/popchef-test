import Product from "./ProductType";
import Ingredient from "./IngredientType";

export default interface ProductWithIngredients extends Product {
  ingredients: Ingredient[];
}
