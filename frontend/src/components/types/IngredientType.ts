import Product from './ProductType';

interface Ingredient {
    id: number;
    name: string;
    products: Product[];
    stock: number;
  }
  
  export default Ingredient;