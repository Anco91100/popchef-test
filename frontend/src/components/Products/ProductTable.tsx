import React from 'react';
import { Skeleton, Space, Table, Tag, Tooltip } from 'antd';
import { useQuery, useMutation } from '@apollo/client';
import { truncate } from 'lodash';
import { Link } from 'react-router-dom';
import { WarningOutlined } from '@ant-design/icons';
import { DELETE_PRODUCT, GET_PRODUCTSWITHINGREDIENTS } from '../../queries/ProductQueries';
import ProductWithIngredients from '../types/ProductWithIngredientsType'

type RecordType = {__typename: string} & ProductWithIngredients;

const ProductTable: React.FC = () => {
    const { loading, error, data } = useQuery(GET_PRODUCTSWITHINGREDIENTS);
    
    const [deleteProduct] = useMutation(DELETE_PRODUCT);

    const onDeleteProduct = (productId: number) => {
        deleteProduct({
            variables: { deleteProductId: productId },
            refetchQueries: [{ query: GET_PRODUCTSWITHINGREDIENTS }],
        });
    };
    const columns = [
        {
            title: 'Name',
            dataIndex: 'name',
            key: 'name',
            render: (text: string, record: RecordType) => {
                return <div className='flex flex-row items-center gap-2'>
                    <Link to={`/product/${record.id}`} state={{ product: record }}>{text}</Link>
                    {record.stock < 50 && <Tooltip title='Attention, bientot la rupture de stock. Passez votre commande !'><WarningOutlined className='text-red-500' /></Tooltip>}
                </div>
            },
        },
        {
            title: 'Description',
            dataIndex: 'description',
            key: 'description',
            render: (text: string) => <div dangerouslySetInnerHTML={{ __html: truncate(text, { length: 35 }) }} />
        },
        {
            title: 'Stock',
            dataIndex: 'stock',
            key: 'stock',
            render: (text: string) => <p>{text}</p>
        },
        {
            title: 'Ingredients',
            key: 'ingredients',
            dataIndex: 'ingredients',
            render: (ingredients: any[]) => (
                <>
                    {ingredients.map((ingredient: { id: number; name: string }) => (
                        <Tag key={ingredient.id}>{ingredient.name}</Tag>
                    ))}
                </>
            ),
        },
        {
            title: 'Action',
            key: 'action',
            render: (_: any, record: RecordType) => (
                <Space size="middle">
                    <button onClick={() => onDeleteProduct(record.id)}>Delete</button>
                    <Link to={`/update/product/${record.id}`} state={{ product: record }}> Edit </Link>
                </Space>
            ),
        },
    ];

    if (loading) {
        return <div><Skeleton /></div>;
    }

    if (error) {
        return <p>Error: {error.message}</p>;
    }

    return <Table columns={columns} dataSource={data?.productsWithIngredients} pagination={{ defaultPageSize: 5, showSizeChanger: true, pageSizeOptions: ['5', '10', '15'] }} className='border-solid border border-gray-200 rounded-md p-5' />;
};

export default ProductTable;
