import { Link, useLocation, Location } from "react-router-dom";
import DescriptionProduct from "../Descriptions/DescriptionProduct";
import { Button } from "antd";
import ProductType from '../types/ProductType'

interface ProductLocation extends Location {
  state: {
    product: ProductType;
  };
}


const Product: React.FC = () => {
    const location: ProductLocation = useLocation();
  return (
    <div className="flex flex-col w-screen h-screen container mx-auto pt-5 text-center">
        <Link to={'/product'} className="flex flex-row items-start text-lg"> Back </Link>
        <DescriptionProduct product={location.state.product} />
        {location.state.product.stock < 50 && <Button className="w-50 flex flex-row text-center self-center" href="/command"> Commander </Button>}
    </div>
  );
};

export default Product;
