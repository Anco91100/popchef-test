import { AppstoreOutlined, HomeOutlined, LoginOutlined, UserOutlined, CrownOutlined } from "@ant-design/icons";
import { Avatar, Divider } from 'antd';
import { Link } from "react-router-dom";
const Navbar: React.FC<{ productSelected?: boolean, homeSelected?: boolean, ingredientSelect?: boolean }> = ({ productSelected, homeSelected, ingredientSelect }) => {
    return (
        <div className=".sidebar-navbar flex flex-row w-[200px] h-full bg-[#101820]">
            <div className=".content flex flex-col justify-between items-start w-[279px] h-screen">
                <div className=".nav flex flex-col items-start p-[32px_0px_0px] gap-6 w-full h-[416px] justify-between">
                    <div className=".header flex flex-row items-center text-center p-[0px_20px_0_24px] w-full h-8">
                        <CrownOutlined className="text-2xl text-[#F2F4F7]" />
                        <h1 className="text-2xl text-[#F2F4F7]">PopChef</h1>
                    </div>
                    <div className=".navigation flex flex-col items-start p-[0px_16px] gap-1 w-full h-[260px]">
                        <Link className={`w-full h-[40px]`} to={'/'}>
                            <div className={`.nav-item flex flex-row items-center p-[8px_12px] gap-[105px] w-full h-[40px] rounded-md cursor-pointer ${homeSelected ? 'bg-[#344054]' : ''}`}>
                                <div className=".content flex flex-row items-center justify-center gap-3">
                                    <HomeOutlined className="text-[#F2F4F7] text-[24px] leading-6 font-medium" />
                                    <h2 className="text-[#F2F4F7] text-base leading-6 font-medium">Home</h2>
                                </div>
                            </div>
                        </Link>
                        <Link className={`w-full h-[40px]`} to={'/product'}>
                            <div className={`.nav-item flex flex-row items-center p-[8px_12px] gap-[105px] w-full h-[40px] rounded-md cursor-pointer ${productSelected ? 'bg-[#344054]' : ''}`}>
                                <div className=".content flex flex-row items-center justify-center gap-3">
                                    <AppstoreOutlined className="text-[#F2F4F7] text-[24px] leading-6 font-medium" />
                                    <h2 className="text-[#F2F4F7] text-base leading-6 font-medium">Produit</h2>
                                </div>
                            </div>
                        </Link>
                        <Link className={`w-full h-[40px]`} to={'/ingredient'}>
                            <div className={`.nav-item flex flex-row items-center p-[8px_12px] gap-[105px] w-full h-[40px] rounded-md cursor-pointer ${ingredientSelect ? 'bg-[#344054]' : ''}`}>
                                <div className=".content flex flex-row items-center justify-center gap-3">
                                    <AppstoreOutlined className="text-[#F2F4F7] text-[24px] leading-6 font-medium" />
                                    <h2 className="text-[#F2F4F7] text-base leading-6 font-medium">Ingredient</h2>
                                </div>
                            </div>
                        </Link>
                    </div>
                </div>
                <div className=".footer flex flex-col items-end justify-end p-[0px_16px_32px] gap-6 w-full h-[449px]">
                    <Divider className="bg-[#475467]" />
                    <div className=".Account flex flex-row w-full h-10">
                        <div className=".avatar-group flex flex-row items-center gap-3 w-full h-full">
                            <Avatar size={40} icon={<UserOutlined />} />
                            <p className="text-xs font-semibold text-white">Popchef Admin</p>
                        </div>
                        <LoginOutlined className=" text-white" size={20} />
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Navbar;
