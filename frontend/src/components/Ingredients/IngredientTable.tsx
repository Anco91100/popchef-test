import React from 'react';
import { Skeleton, Space, Table, Tooltip } from 'antd';
import { useQuery, useMutation } from '@apollo/client';
import { Link } from 'react-router-dom';
import { WarningOutlined } from '@ant-design/icons';
import { GET_INGREDIENTS, DELETE_INGREDIENT } from '../../queries/IngredientQueries';
import Ingredient from '../types/IngredientType';

interface RecordType extends Ingredient {
    __typename: string
}

const IngredientTable: React.FC = () => {
    const { loading, error, data } = useQuery(GET_INGREDIENTS);
    const [deleteIngredient] = useMutation(DELETE_INGREDIENT);

    const onDeleteIngredient = (ingredientId: number) => {
        deleteIngredient({
            variables: { deleteIngredientId: ingredientId },
            refetchQueries: [{ query: GET_INGREDIENTS }],
        });
    };
    const columns = [
        {
            title: 'Name',
            dataIndex: 'name',
            key: 'name',
            render: (text: string, record: RecordType) => {
                return <div className='flex flex-row items-center gap-2'>
                <Link to={`/ingredient/${record.id}`} state={{ ingredient: record }}>{text}</Link>
                {record.stock < 50 && <Tooltip title='Attention, bientot la rupture de stock. Passez votre commande !'><WarningOutlined className='text-red-500' /></Tooltip>}
                </ div>
            },
        },
        {
            title: 'Stock',
            dataIndex: 'stock',
            key: 'stock',
            render: (text: string) => <p>{text}</p>
        },
        {
            title: 'Action',
            key: 'action',
            render: (_: any, record: RecordType) => (
                <Space size="middle">
                    <button onClick={() => onDeleteIngredient(record.id)}>Delete</button>
                    <Link to={`/update/ingredient/${record.id}`} state={{ingredient: record}}> Edit </Link>
                </Space>
            ),
        },
    ];

    if (loading) {
        return <div><Skeleton /></div>;
    }

    if (error) {
        return <p>Error: {error.message}</p>;
    }

    return <Table columns={columns} dataSource={data?.ingredients} pagination={{ defaultPageSize: 5, showSizeChanger: true, pageSizeOptions: ['5', '10', '15']}} className='border-solid border border-gray-200 rounded-md p-5' />;
};

export default IngredientTable;
