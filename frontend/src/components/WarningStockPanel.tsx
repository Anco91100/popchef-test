import { ExclamationCircleOutlined } from "@ant-design/icons";
import { useQuery } from "@apollo/client";
import { Skeleton } from "antd";
import {GET_LOWSTOCKPRODUCTS} from '../queries/ProductQueries';
import {GET_LOWSTOCKINGREDIENTS} from '../queries/IngredientQueries';

const WarningStockPanel: React.FC = () => {
    const {error: errorProducts, loading: loadingProducts, data: dataProducts} = useQuery(GET_LOWSTOCKPRODUCTS);
    const {error: errorIngredients, loading: loadingIngredients, data: dataIngredients} = useQuery(GET_LOWSTOCKINGREDIENTS);
    if (loadingProducts || loadingIngredients) {
        return <div><Skeleton /></div>;
      }
    
      if (errorProducts|| errorIngredients ) {
        const error = errorIngredients || errorProducts || {message: ''};
        return <div>Error occurred: {error.message}</div>;
      }
    
      if(!dataProducts || !dataIngredients){
        return <div>No data fetched</div>;
      }
    return (
        dataIngredients.lowStockIngredients.length > 0 || dataProducts.lowStockProducts.length > 0 ? <div className="flex flex-col w-full h-full border-solid border border-gray-200 rounded-md p-5 items-center gap-8">
            <ExclamationCircleOutlined className="text-[200px] text-red-500" />
            {dataProducts && <p>{dataProducts.lowStockProducts.length} produits sont bientot en rupture de stock, commendez vos produits</p>}
            {dataIngredients && <p>{dataIngredients.lowStockIngredients.length} ingredients sont bientot en rupture de stock, commendez vos ingredients</p>}
        </div> : <></>
    );
};

export default WarningStockPanel;
