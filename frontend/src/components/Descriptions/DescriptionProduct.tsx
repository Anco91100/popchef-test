import React from 'react';
import { Badge, Descriptions } from 'antd';
import Product from '../types/ProductType';
import Ingredient from '../types/IngredientType';

const DescriptionProduct: React.FC<{ product: Product }> = (props) => (
    <Descriptions title="Product Info" bordered>
        <Descriptions.Item label="Nom du produit" span={2}>
            <p>{props.product.name}</p>
        </Descriptions.Item>
        <Descriptions.Item label="Stock du produit" span={1}>
        {props.product.stock < 50 ? <Badge status="warning" text="Commandez ce produit, et evitez la rupture de stock" /> : <Badge status="success" text="En stock" />}
        </Descriptions.Item>
        <Descriptions.Item label="Description du produit" span={3}>
            <div dangerouslySetInnerHTML={{ __html: props.product.description }} />
        </Descriptions.Item>
        <Descriptions.Item label="Ingrédients contenus dans le produit" span={1}>
            {props.product.ingredients.map((ingredient: Ingredient) => (
                <p key={ingredient.id}>{ingredient.name}</p>
            ))}
        </Descriptions.Item>
    </Descriptions>
);

export default DescriptionProduct;
