import React from 'react';
import { Badge, Descriptions } from 'antd';
import Ingredient from '../types/IngredientType';

const DescriptionIngredient: React.FC<{ ingredient: Ingredient }> = (props) => (
    <Descriptions title="Ingredient Info" bordered>
        <Descriptions.Item label="Nom de l'ingredient" span={2}>
            <p>{props.ingredient.name}</p>
        </Descriptions.Item>
        <Descriptions.Item label="Stock de l'ingredient" span={1}>
            {props.ingredient.stock < 50 ? <Badge status="warning" text="Commandez ce produit, et evitez la rupture de stock" /> : <Badge status="success" text="En stock" />}
        </Descriptions.Item>
    </Descriptions>
);

export default DescriptionIngredient;
