import React from 'react';
import { render, screen } from '@testing-library/react';
import HomePage from '../../pages/HomePage';
import { BrowserRouter as Router } from "react-router-dom";
import { ApolloProvider } from '@apollo/client';
import { apolloClient } from "../../App";

test('renders HomePage', () => {
  render(
    <ApolloProvider client={apolloClient}>
      <Router basename='/'>
      <HomePage />
      </Router>
    </ApolloProvider>
  );

  const navbarElement = screen.getByTestId('navbar');
  const mainBoardElement = screen.getByTestId('mainboard');

  expect(navbarElement).toBeInTheDocument();
  expect(mainBoardElement).toBeInTheDocument();
});
