import React from 'react';
import { render, screen } from '@testing-library/react';
import ProductPage from '../../pages/ProductPage';
import { BrowserRouter as Router } from "react-router-dom";
import { ApolloProvider } from '@apollo/client';
import { apolloClient } from "../../App";

test('renders HomePage', () => {
  render(
    <ApolloProvider client={apolloClient}>
      <Router>
      <ProductPage />
      </Router>
    </ApolloProvider>
  );

  const navbarElement = screen.getByTestId('navbar');
  const mainBoardElement = screen.getByTestId('mainboardproduct');

  expect(navbarElement).toBeInTheDocument();
  expect(mainBoardElement).toBeInTheDocument();
});
