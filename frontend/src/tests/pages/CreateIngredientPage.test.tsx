import { render, screen, fireEvent, waitFor } from '@testing-library/react';
import { MockedProvider } from '@apollo/client/testing';
import CreateIngredientPage from '../../pages/CreateIngredientPage';


describe('CreateIngredientPage', () => {
  test('renders create ingredient form', async () => {
    render(
        <CreateIngredientPage />
    );

    expect(screen.getByLabelText('Nom de l\'ingrédient')).toBeInTheDocument();
    expect(screen.getByLabelText('Stock de l\'ingrédient')).toBeInTheDocument();

    expect(screen.getByText('Save')).toBeInTheDocument();
  });
});
