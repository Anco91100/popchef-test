import { render, screen } from '@testing-library/react';
import CommandPage from '../../pages/CommandPage';

describe('Command', () => {
  test('renders command tracking page', () => {
    render(<CommandPage />);

    expect(screen.getByText('Votre commande a été pris en compte !')).toBeInTheDocument();

    expect(screen.getByText('Commande en cours de validation')).toBeInTheDocument();
    expect(screen.getByText('Commande validé')).toBeInTheDocument();
    expect(screen.getByText('En cours de livraison')).toBeInTheDocument();
  });
});
