import { render, screen, fireEvent } from '@testing-library/react';
import { MockedProvider } from '@apollo/client/testing';
import { BrowserRouter } from 'react-router-dom';
import UpdateProductPage from '../../pages/UpdateProductPage';
import { GET_PRODUCTSWITHINGREDIENTS, UPDATE_PRODUCTWITHINGREDIENTS } from '../../queries/ProductQueries';
import { GET_INGREDIENTS } from '../../queries/IngredientQueries';

const mockIngredientData = {
  ingredients: [
    { id: 1, name: 'Tomato', stock: 10 },
    { id: 2, name: 'Mozzarella', stock: 5 },
    { id: 3, name: 'Basil', stock: 2 },
  ],
};

const mockProductData = {
  description: 'Product description',
  name: 'Product name',
  stock: '10',
  id: 1,
  ingredients: {
    name: 'Tomato',
    id: 1
  }
};

const mocks = [
  {
    request: { query: GET_INGREDIENTS },
    result: { data: mockIngredientData },
  },
  {
    request: { query: GET_PRODUCTSWITHINGREDIENTS, variables: { productId: 1 } },
    result: { data: { product: mockProductData } },
  },
  {
    request: {
      query: UPDATE_PRODUCTWITHINGREDIENTS,
      variables: { data: { name: 'Updated Product', description: 'Updated description', stock: '20' }, updateProductWithIngredientsId: 1 },
    },
    result: { data: { updateProductWithIngredients: { description: 'Updated description', stock: '20', name: 'Updated Product' } } },
  },
];

describe('UpdateProductPage', () => {
  test('renders update product form and submits successfully', async () => {
    render(
      <MockedProvider mocks={mocks} addTypename={false}>
        <BrowserRouter>
          <UpdateProductPage />
        </BrowserRouter>
      </MockedProvider>
    );

    expect(screen.getByText('Modifiez le produit')).toBeInTheDocument();
    expect(screen.getByLabelText('Name')).toBeInTheDocument();
    expect(screen.getByLabelText('Description')).toBeInTheDocument();
    expect(screen.getByLabelText('Stock')).toBeInTheDocument();
    expect(screen.getByLabelText('Ingredients')).toBeInTheDocument();
    expect(screen.getByRole('button', { name: 'Update' })).toBeInTheDocument();

    await screen.findByText('Tomato');

    fireEvent.change(screen.getByLabelText('Name'), { target: { value: 'Updated Product' } });
    fireEvent.change(screen.getByLabelText('Description'), { target: { value: 'Updated description' } });
    fireEvent.change(screen.getByLabelText('Stock'), { target: { value: '20' } });

    fireEvent.select(screen.getByLabelText('Ingredients'), [screen.getByText('Tomato')]);

    fireEvent.click(screen.getByRole('button', { name: 'Update' }));

    await screen.findByText('Super tu as modifié un produit !');
  });
});
