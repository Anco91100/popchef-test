import { render, screen } from '@testing-library/react';
import IngredientPage from '../../pages/IngredientPage';

describe('IngredientPage', () => {
  test('renders navbar and main board ingredient', () => {
    render(<IngredientPage />);

    expect(screen.getByTestId('navbar')).toBeInTheDocument();

    expect(screen.getByTestId('mainboardingredient')).toBeInTheDocument();
  });
});
