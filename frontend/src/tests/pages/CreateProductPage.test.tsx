import { render, screen, fireEvent } from '@testing-library/react';
import CreateProductPage from '../../pages/CreateProductPage';

describe('CreateProductPage', () => {
  test('renders create product form', () => {
    render(<CreateProductPage />);

    expect(screen.getByLabelText('Nom du produit')).toBeInTheDocument();
    expect(screen.getByLabelText('Description du produit')).toBeInTheDocument();
    expect(screen.getByLabelText('Stock')).toBeInTheDocument();
    expect(screen.getByLabelText('Ingrédients contenus dans le produit')).toBeInTheDocument();

    expect(screen.getByText('Save')).toBeInTheDocument();
  });
});
