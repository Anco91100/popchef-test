import { render, screen, fireEvent } from '@testing-library/react';
import { MockedProvider } from '@apollo/client/testing';
import { BrowserRouter } from 'react-router-dom';
import UpdateIngredientPage from '../../pages/UpdateIngredientPage';
import { GET_INGREDIENTS, UPDATE_INGREDIENT } from '../../queries/IngredientQueries';

const mockIngredientData = {
  name: 'Tomato',
  stock: '10',
};

const mocks = [
  {
    request: { query: GET_INGREDIENTS },
    result: { data: { ingredient: mockIngredientData } },
  },
  {
    request: {
      query: UPDATE_INGREDIENT,
      variables: { input: { name: 'Updated Ingredient', stock: '20' }, updateIngredientId: 1 },
    },
    result: { data: { updateIngredient: { name: 'Updated Ingredient', stock: '20' } } },
  },
];

describe('UpdateIngredientPage', () => {
  test('renders update ingredient form and submits successfully', async () => {
    render(
      <MockedProvider mocks={mocks} addTypename={false}>
        <BrowserRouter>
          <UpdateIngredientPage />
        </BrowserRouter>
      </MockedProvider>
    );

    expect(screen.getByText('Modifiez l\'ingrédient')).toBeInTheDocument();
    expect(screen.getByLabelText('Name')).toBeInTheDocument();
    expect(screen.getByLabelText('Stock')).toBeInTheDocument();
    expect(screen.getByRole('button', { name: 'Update' })).toBeInTheDocument();

    fireEvent.change(screen.getByLabelText('Name'), { target: { value: 'Updated Ingredient' } });
    fireEvent.change(screen.getByLabelText('Stock'), { target: { value: '20' } });

    fireEvent.click(screen.getByRole('button', { name: 'Update' }));

    await screen.findByText('Super tu as modifié un ingrédient');
  });
});
