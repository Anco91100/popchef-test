import { render, screen } from '@testing-library/react';
import Navbar from '../../components/Navbar';

describe('Navbar', () => {
  test('renders navigation links correctly', () => {
    render(<Navbar productSelected={true} />);

    expect(screen.getByText('Produit')).toHaveClass('bg-[#344054]');

    expect(screen.getByText('Home')).toBeInTheDocument();
    expect(screen.getByText('Ingredient')).toBeInTheDocument();
  });

  test('renders logout button correctly', () => {
    render(<Navbar />);

    const logoutButton = screen.getByLabelText('Logout');
    expect(logoutButton).toBeInTheDocument();
  });
});
