import React from 'react';
import { render } from '@testing-library/react';
import DescriptionProduct from '../../components/Descriptions/DescriptionProduct';

describe('DescriptionProduct', () => {
  test('renders product details', () => {
    const product = {
      name: 'Product 1',
      id:1,
      stock: 60,
      description: 'This is a product description.',
      ingredients: [
        {
            name: 'Ingredient 1',
            stock: 60,
            id: 1,
            products: []
          },
          {
            name: 'Ingredient 2',
            stock: 60,
            id: 2,
            products: []
          },
      ],
    };

    const { getByText } = render(<DescriptionProduct product={product} />);

    expect(getByText('Nom du produit')).toBeInTheDocument();
    expect(getByText('Product 1')).toBeInTheDocument();
    expect(getByText('Stock du produit')).toBeInTheDocument();
    expect(getByText('En stock')).toBeInTheDocument();
    expect(getByText('Description du produit')).toBeInTheDocument();
    expect(getByText('This is a product description.')).toBeInTheDocument();
    expect(getByText('Ingrédients contenus dans le produit')).toBeInTheDocument();
    expect(getByText('Ingredient 1')).toBeInTheDocument();
    expect(getByText('Ingredient 2')).toBeInTheDocument();
  });

  test('renders warning badge when stock is low', () => {
    const product = {
      name: 'Product 2',
      id: 2,
      stock: 30,
      description: 'This is another product description.',
      ingredients: [
        {
            name: 'Ingredient 3',
            stock: 60,
            id: 3,
            products: []
          },
          {
            name: 'Ingredient 4',
            stock: 60,
            id: 4,
            products: []
          },
      ],
    };

    const { getByText } = render(<DescriptionProduct product={product} />);

    expect(getByText('Nom du produit')).toBeInTheDocument();
    expect(getByText('Product 2')).toBeInTheDocument();
    expect(getByText('Stock du produit')).toBeInTheDocument();
    expect(getByText('Commandez ce produit, et evitez la rupture de stock')).toBeInTheDocument();
    expect(getByText('Description du produit')).toBeInTheDocument();
    expect(getByText('This is another product description.')).toBeInTheDocument();
    expect(getByText('Ingrédients contenus dans le produit')).toBeInTheDocument();
    expect(getByText('Ingredient 3')).toBeInTheDocument();
    expect(getByText('Ingredient 4')).toBeInTheDocument();
  });
});
