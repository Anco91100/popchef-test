import { render, screen } from '@testing-library/react';
import WarningStockPanel from '../../components/WarningStockPanel';

describe('WarningStockPanel', () => {
  test('renders no data message when no data is fetched', () => {
    render(<WarningStockPanel />);

    expect(screen.getByText('No data fetched')).toBeInTheDocument();
  });
});
