import React from 'react';
import { render } from '@testing-library/react';
import DescriptionIngredient from '../../components/Descriptions/DescriptionIngredient';

describe('DescriptionIngredient', () => {
  test('renders ingredient details', () => {
    const ingredient = {
      name: 'Ingredient 1',
      stock: 60,
      id: 1,
      products: []
    };

    const { getByText } = render(<DescriptionIngredient ingredient={ingredient} />);

    expect(getByText("Nom de l'ingredient")).toBeInTheDocument();
    expect(getByText("Ingredient 1")).toBeInTheDocument();
    expect(getByText("Stock de l'ingredient")).toBeInTheDocument();
    expect(getByText("En stock")).toBeInTheDocument();
  });

  test('renders warning badge when stock is low', () => {
    const ingredient = {
      name: 'Ingredient 2',
      stock: 30,
      id: 1,
      products: []
    };

    const { getByText } = render(<DescriptionIngredient ingredient={ingredient} />);

    expect(getByText("Nom de l'ingredient")).toBeInTheDocument();
    expect(getByText("Ingredient 2")).toBeInTheDocument();
    expect(getByText("Stock de l'ingredient")).toBeInTheDocument();
    expect(getByText("Commandez ce produit, et evitez la rupture de stock")).toBeInTheDocument();
  });
});
