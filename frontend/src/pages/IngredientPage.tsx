import MainBoardIngredient from "../components/Mainboards/MainBoardIngredient";
import Navbar from "../components/Navbar";

const IngredientPage: React.FC = () => {
  return (
    <div className="w-screen h-screen flex flex-row items-start bg-[#101820]">
      <Navbar data-testid="navbar" ingredientSelect={true} />
      <MainBoardIngredient data-testid="mainboardingredient" />
    </div>
  );
};

export default IngredientPage;
