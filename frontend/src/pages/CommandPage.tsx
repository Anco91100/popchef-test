import Navbar from "../components/Navbar";
import {Steps} from 'antd'
const Command: React.FC = () => {
  return (
    <div className="w-screen h-screen flex flex-row items-start bg-[#101820]">
        <Navbar />
        <div className=".main-wrap flex flex-col items-start p-[12px_0px_0px] w-full h-full">
            <div className="flex flex-col items-start p-[32px_0px_48px] gap-8 h-full w-full rounded-[40px_0px_0px_40px] bg-white">
                <div className=".header-section flex flex-col items-start gap-6 w-full h-[66px]">
                    <div className=".container flex flex-col items-start p-[0px_32px] gap-6 h-full w-full">
                        <div className=".content flex flex-row items-center justify-center gap-4 h-full w-full">
                            <h2 className="w-[875px] h-10 text-2xl text-[#101828 font-mdeium]" > Votre commande a été pris en compte ! </h2>
                        </div>
                    </div>
                </div>
                <div className="w-full h-full flex flex-row gap-5 p-5 justify-center">
                <Steps size="default" current={0} items={[{title: 'Commande en cours de validation'}, {title: 'Commande validé'}, {title: 'En cours de livraison'}]} />
                </div>
            </div>
        </div>
    </div>
  );
};

export default Command;
