import MainBoardProduct from "../components/Mainboards/MainBoardProduct";
import Navbar from "../components/Navbar";


const ProductPage: React.FC = () => {
  return (
    <div className="w-screen h-screen flex flex-row items-start bg-[#101820]">
      <Navbar data-testid="navbar" productSelected={true} />
      <MainBoardProduct data-testid="mainboardproduct" />
    </div>
  );
};

export default ProductPage;
