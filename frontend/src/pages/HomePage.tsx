import MainBoard from "../components/Mainboards/Mainboard";
import Navbar from "../components/Navbar";

const HomePage: React.FC = () => {
  return (
    <div className="w-screen h-screen flex flex-row items-start bg-[#101820]">
      <Navbar data-testid="navbar" homeSelected={true}/>
      <MainBoard data-testid="mainboard" />
    </div>
  );
};

export default HomePage;
