import { Formik, Field, ErrorMessage } from 'formik';
import toast, { Toaster } from 'react-hot-toast';
import { useMutation, useQuery } from "@apollo/client";
import React from 'react'
import { Skeleton, Empty } from 'antd';
import MarkdownIt from 'markdown-it';
import * as Yup from 'yup';
import { CREATE_PRODUCTWITHINGREDIENTS, GET_PRODUCTSWITHINGREDIENTS } from '../queries/ProductQueries';
import { GET_INGREDIENTS } from '../queries/IngredientQueries';

const markdown = new MarkdownIt({
    html: true,
    breaks: true,
    linkify: true,
    typographer: true,
});

type ProductWithIngredientsInput = {
    name: string,
    description: string,
    stock: string,
    ingredients?: []
};

const CreateProductPage: React.FC = () => {
    const [createProduct] = useMutation(CREATE_PRODUCTWITHINGREDIENTS);
    const { loading, error, data } = useQuery(GET_INGREDIENTS);
    const { refetch: refetchProducts } = useQuery(GET_PRODUCTSWITHINGREDIENTS);
    const validationSchema = Yup.object().shape({
        ingredients: Yup.array().min(1, 'Select at least one ingredient'),
    });
    const handleSubmit = async (values: ProductWithIngredientsInput) => {
        try {
            await validationSchema.validate(values, { abortEarly: false });
            const enrichedDescription = markdown.render(values.description);

            const productData = {
                ...values,
                stock: parseFloat(values.stock),
                description: enrichedDescription,
            };
            await createProduct({ variables: { data: productData } });
            toast.success("Super tu as ajouté un produit !");
            await refetchProducts();
        } catch (error) {
            if (error instanceof Yup.ValidationError) {
                toast.error('Une erreur s\'est produite pendant la saisie du formulaire');
            }
            console.error("Error creating product:", error);
        }
    };
    if (loading) {
        return <div><Skeleton /></div>;
    }

    if (error) {
        return <div>Error occurred: {error.message}</div>;
    }

    if (!data) {
        return <div>No data fetched</div>;
    }
    return (
        <div className='flex flex-col w-full h-full items-start gap-4 rounded-2xl' >
            {data.ingredients.length <= 0 && <Empty className='w-full h-full'> <p> Créez un ingrédient pour créer un produit </p></Empty>}
            {data.ingredients.length > 0 && <><Toaster /><Formik
                initialValues={{ name: '', description: '', stock: '' }}
                validate={(values: ProductWithIngredientsInput) => {
                    const errors: Partial<{ stock: string, description: string, name: string, ingredients: string }> = {};

                    if (!values.name) {
                        errors.name = 'Required';
                    }

                    if (!values.description) {
                        errors.description = 'Required';
                    }

                    if (!values.stock) {
                        errors.stock = 'Required';
                    }

                    if (values?.ingredients?.length === 0) {
                        errors.ingredients = 'At least one ingredient must be selected';
                    }

                    return errors;
                }}
                onSubmit={handleSubmit}
            >
                {({ handleSubmit }) => (
                    <form className='w-full h-full' onSubmit={handleSubmit}>
                        <div className='flex flex-col w-full h-[500px] items-start p-4 gap-3 bg-white shadow-[0px_4px_20px_rgba(0,0,0,0.08)]'>
                            <div className='flex flex-col min-w-full items-start h-16 gap-2'>
                                <h2> Ajoutez un produit</h2>
                                <div className='border border-solid border-[rgba(188,202,220,0.5)] w-full'></div>
                            </div>
                            <div className="flex flex-col min-w-full items-start h-24 gap-1">
                                <h2 className="text-[#627D98] font-normal text-xs">Nom du produit</h2>
                                <Field className="rounded-sm text-[#0E1823] border border-solid border-[#BCCADC] w-full h-9 p-3 gap-2 items-center" type="text" name="name" />
                                <ErrorMessage name="name" component="div" className="text-red-500 text-xs" />
                            </div>
                            <div className="flex flex-col min-w-full items-start h-24 gap-1">
                                <h2 className="text-[#627D98] font-normal text-xs">Description du produit</h2>
                                <Field as="textarea" className="rounded-sm text-[#0E1823] border border-solid border-[#BCCADC] w-full h-9 p-3 gap-2 items-center" type="text" name="description" />
                                <ErrorMessage name="description" component="div" className="text-red-500 text-xs" />
                                <p className="text-gray-500 text-xs"> ** mot ** : gras, _ mot _ : italique, ~~ mot ~~: mot barré, ^ chiffre ^ : texte en exposant, ~ mot ~: texte en indice, # : Titre 1 / ## : Titre 2 etc , * ou + ou - suivi d'un espace et votre mot : liste à puces </p>

                            </div>

                            <div className="flex flex-col min-w-full items-start h-24 gap-1">
                                <h2 className="text-[#627D98] font-normal text-xs">Stock</h2>
                                <Field className="rounded-sm text-[#0E1823] border border-solid border-[#BCCADC] w-full h-9 p-3 gap-2 items-center" type="text" name="stock" />
                                <ErrorMessage name="stock" component="div" className="text-red-500 text-xs" />
                            </div>
                            <div className="flex flex-col min-w-full items-start h-24 gap-1">
                                <h2 className="text-[#627D98] font-normal text-xs">Ingrédients contenus dans le produit</h2>
                                <Field
                                    as="select"
                                    className="rounded-sm text-[#0E1823] bg-white border border-solid border-[#BCCADC] w-full h-auto p-3 gap-2 items-center"
                                    name="ingredients"
                                    multiple
                                    required
                                >
                                    {data.ingredients.map((ingredient: any) => (
                                        <option key={ingredient.id} value={ingredient.id}>{ingredient.name}</option>
                                    ))}
                                </Field>
                                <ErrorMessage name="ingredients" component="div" className="text-red-500 text-xs" />
                            </div>
                            <p className="text-gray-500 text-xs"> CTRL + Clic pour une sélection multiple ou CTRL + Clic pour désélectionner</p>
                        </div>
                        <div className='flex flex-row justify-end items-start p-4 h-7 w-full'>
                            <button type="submit" className='flex flex-row items-center justify-center p-4 gap-2 w-20 h-7 rounded-sm  border-solid border-[#104EE9] bg-[#104EE9] text-white text-center text-sm font-normal'> Save </button>
                        </div>
                    </form>
                )}
            </Formik></>}
        </div>
    );
};

export default CreateProductPage;
