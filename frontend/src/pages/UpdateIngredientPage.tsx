import { Formik, Field, ErrorMessage } from 'formik';
import toast, { Toaster } from 'react-hot-toast';
import { useMutation, useQuery } from "@apollo/client";
import React from 'react'
import { Link, useNavigate } from "react-router-dom";
import { useParams } from 'react-router-dom';
import { useLocation } from "react-router-dom";
import { GET_INGREDIENTS, UPDATE_INGREDIENT } from '../queries/IngredientQueries';

type IngredientInput = {
    id?: number,
    name: string,
    stock: string
}
const UpdateIngredientPage: React.FC = () => {
    const [updateIngredient] = useMutation(UPDATE_INGREDIENT);
    const navigate = useNavigate();
    const { ingredientId } = useParams<{ ingredientId: string }>();
    const location = useLocation();
    const initialIngredientValues: IngredientInput = location.state?.ingredient || {};
    const { refetch } = useQuery(GET_INGREDIENTS, {
        variables: { ingredientId: initialIngredientValues.id },
      });
    const handleSubmit = async (values: IngredientInput) => {
        try {
            const ingredientIdValue = ingredientId ? parseFloat(ingredientId) : undefined;
            const ingredientData = {
                ...values,
                stock: parseFloat(values.stock)
              };
            await updateIngredient({ variables: { input: ingredientData, updateIngredientId: ingredientIdValue } });
            toast.success("Super tu as modifié un ingrédient");
            await refetch();
            navigate('/ingredient');
        } catch (error) {
            console.error("Error updating ingredient:", error);
        }
    };
    return (
        <div className="container mx-auto pt-5 text-center">
            <Link to={'/ingredient'} className="flex flex-row items-start text-lg"> Back </Link>
            <div className='flex flex-col w-full h-full items-start gap-4 rounded-2xl' >
                <Toaster />
                <Formik 
                initialValues={{ name: initialIngredientValues.name, stock:initialIngredientValues.stock }} 
                onSubmit={handleSubmit}
                validate={(values: IngredientInput) => {
                    const errors: Partial<{ stock: string, name: string }> = {};
      
                    if (!values.name) {
                        errors.name = 'Required';
                    }
      
                    if (!values.stock) {
                        errors.stock = 'Required';
                    }
      
                    return errors;
                }}
                >
                    {({ handleSubmit }) => (
                        <form className='w-full h-full' onSubmit={handleSubmit}>
                            <div className='flex flex-col w-full h-[250px] items-start p-4 bg-white shadow-[0px_4px_20px_rgba(0,0,0,0.08)]' >
                                <div className='flex flex-col min-w-full items-start h-16 gap-2'>
                                    <h2>Modifiez l'ingrédient</h2>
                                    <div className='border border-solid border-[rgba(188,202,220,0.5)] w-full'></div>
                                </div>
                                <div className="flex flex-col min-w-full items-start h-24 gap-1">
                                    <h2 className="text-[#627D98] font-normal text-xs">Name</h2>
                                    <Field className="rounded-sm text-[#0E1823] border border-solid border-[#BCCADC] w-full h-9 p-3 gap-2 items-center" type="text" name="name" />
                                    <ErrorMessage name="lastName" component="div" className="text-red-500 text-xs" />
                                </div>
                                <div className="flex flex-col min-w-full items-start h-24 gap-1">
                                <h2 className="text-[#627D98] font-normal text-xs">Stock</h2>
                                <Field className="rounded-sm text-[#0E1823] border border-solid border-[#BCCADC] w-full h-9 p-3 gap-2 items-center" type="text" name="stock" />
                                <ErrorMessage name="stock" component="div" className="text-red-500 text-xs" />
                            </div>
                            </div>
                            <div className='flex flex-row justify-end items-start p-4 h-7 w-full'>
                                <button type="submit" className='flex flex-row items-center justify-center p-4 gap-2 w-20 h-7 rounded-sm  border-solid border-[#104EE9] bg-[#104EE9] text-white text-center text-sm font-normal'> Update </button>
                            </div>
                        </form>
                    )}
                </Formik>
            </div>
        </div>
    );
};

export default UpdateIngredientPage;
