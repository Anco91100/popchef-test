import { useNavigate } from "react-router-dom";

const NotFoundPage: React.FC = () => {
  const navigate = useNavigate();

  const goToHomePage = () => {
    navigate("/");
  };

  return (
    <div className="container mx-auto pt-5 text-center">
      <h1 className="text-2xl">Erreur 404</h1>
      <div className="mt-5">
        <button onClick={goToHomePage} className="bg-emerald-800 hover:bg-emerald-700 text-white px-5 py-2 rounded-2xl">
          Revenir à l'accueil
        </button>
      </div>
    </div>
  );
};

export default NotFoundPage;
