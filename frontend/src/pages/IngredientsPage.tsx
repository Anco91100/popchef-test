import { Link } from "react-router-dom";
import IngredientTable from "../components/Ingredients/IngredientTable";

const IngredientsPage: React.FC = () => {
    return (
        <div className="flex flex-col w-screen h-screen container mx-auto pt-5 text-center">
            <Link to={'/'} className="flex flex-row items-start text-lg"> Back </Link>
            <h1 className="text-2xl">Bonjour !</h1>
            <div className="mt-16 w-full h-full flex flex-col text-center">
                {/* <Products /> */}
                {/* <Ingredients /> */}
                <IngredientTable />
            </div>
        </div>
    );
};

export default IngredientsPage;
