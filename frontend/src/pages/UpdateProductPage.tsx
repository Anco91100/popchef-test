import { Formik, Field, ErrorMessage } from 'formik';
import toast, { Toaster } from 'react-hot-toast';
import { useMutation, useQuery } from "@apollo/client";
import React from 'react'
import { Link, useNavigate } from "react-router-dom";
import { useParams } from 'react-router-dom';
import { useLocation } from "react-router-dom";
import { Skeleton } from 'antd';
import MarkdownIt from 'markdown-it';
import { UPDATE_PRODUCTWITHINGREDIENTS, GET_PRODUCTSWITHINGREDIENTS } from '../queries/ProductQueries';
import { GET_INGREDIENTS } from '../queries/IngredientQueries';

const markdown = new MarkdownIt({
    html: true,
    breaks: true,
    linkify: true,
    typographer: true,
  });

type ProductWithIngredientsInput = {
    name: string,
    description: string,
    stock: string,
    ingredients?: []
};

const UpdateProductPage: React.FC = () => {
    const [updateProduct] = useMutation(UPDATE_PRODUCTWITHINGREDIENTS);
    const { loading: ingredientLoading, data: ingredientData } = useQuery(GET_INGREDIENTS);

    const navigate = useNavigate();
    const { productId } = useParams<{ productId: string }>();
    const location = useLocation();
    const initialProductValues = location.state?.product || {};
    const productIdValue = productId ? parseFloat(productId) : undefined;
    const { refetch } = useQuery(GET_PRODUCTSWITHINGREDIENTS, {
        variables: { productId: productIdValue },
    });
    const handleSubmit = async (values: ProductWithIngredientsInput) => {
        try {
            const enrichedDescription = markdown.render(values.description);

            const productData = {
                ...values,
                stock: parseFloat(values.stock),
                description: enrichedDescription,
              };
            await updateProduct({ variables: { data: productData, updateProductWithIngredientsId: productIdValue } });
            toast.success("Super tu as modifié un produit !");
            await refetch();
            navigate('/product');
        } catch (error) {
            console.error("Error updating product:", error);
        }
    };
    return (
        <div className="container mx-auto pt-5 text-center">
            <Link to={'/product'} className="flex flex-row items-start text-lg"> Back </Link>
            <div className='flex flex-col w-full h-full items-start gap-4 rounded-2xl' >
                <Toaster />
                <Formik 
                initialValues={{ name: initialProductValues.name, description: initialProductValues.description, stock: initialProductValues.stock }} 
                onSubmit={handleSubmit}
                validate={(values: ProductWithIngredientsInput) => {
                    const errors: Partial<{ stock: string, description: string, name: string, ingredients: string }> = {};

                    if (!values.name) {
                        errors.name = 'Required';
                    }

                    if (!values.description) {
                        errors.description = 'Required';
                    }

                    if (!values.stock) {
                        errors.stock = 'Required';
                    }

                    if (values?.ingredients?.length === 0) {
                        errors.ingredients = 'At least one ingredient must be selected';
                    }

                    return errors;
                }}
                >
                    {({ handleSubmit }) => (
                        <form className='w-full h-full' onSubmit={handleSubmit}>
                            <div className='flex flex-col w-full h-[300px] items-start p-4 bg-white shadow-[0px_4px_20px_rgba(0,0,0,0.08)]' >
                                <div className='flex flex-col min-w-full items-start h-16 gap-2'>
                                    <h2>Modifiez le produit</h2>
                                    <div className='border border-solid border-[rgba(188,202,220,0.5)] w-full'></div>
                                </div>
                                <div className="flex flex-col min-w-full items-start h-24 gap-1">
                                    <h2 className="text-[#627D98] font-normal text-xs">Name</h2>
                                    <Field className="rounded-sm text-[#0E1823] border border-solid border-[#BCCADC] w-full h-9 p-3 gap-2 items-center" type="text" name="name" />
                                    <ErrorMessage name="lastName" component="div" className="text-red-500 text-xs" />
                                </div>
                                <div className="flex flex-col min-w-full items-start h-24 gap-1">
                                    <h2 className="text-[#627D98] font-normal text-xs">Description</h2>
                                    <Field as="textarea" className="rounded-sm text-[#0E1823] border border-solid border-[#BCCADC] w-full h-9 p-3 gap-2 items-center" type="text" name="description" />
                                    <ErrorMessage name="lastName" component="div" className="text-red-500 text-xs" />
                                </div>
                                <div className="flex flex-col min-w-full items-start h-24 gap-1">
                                    <h2 className="text-[#627D98] font-normal text-xs">Stock</h2>
                                    <Field className="rounded-sm text-[#0E1823] border border-solid border-[#BCCADC] w-full h-9 p-3 gap-2 items-center" type="text" name="stock" />
                                    <ErrorMessage name="stock" component="div" className="text-red-500 text-xs" />
                                </div>
                                <div className="flex flex-col min-w-full items-start h-24 gap-1">
                                    <h2 className="text-[#627D98] font-normal text-xs">Ingredients</h2>
                                    <Field
                                        as="select"
                                        className="rounded-sm text-[#0E1823] bg-white border border-solid border-[#BCCADC] w-full h-auto p-3 gap-2 items-center"
                                        name="ingredients"
                                        multiple
                                    >
                                        {ingredientLoading && <Skeleton />}
                                        {ingredientData && ingredientData.ingredients.map((ingredient: any) => (
                                            <option key={ingredient.id} value={ingredient.id} >{ingredient.name}</option>
                                        ))}
                                    </Field>
                                    <ErrorMessage name="ingredients" component="div" className="text-red-500 text-xs" />
                                </div>
                            </div>
                            <div className='flex flex-row justify-end items-start p-4 h-7 w-full'>
                                <button type="submit" className='flex flex-row items-center justify-center p-4 gap-2 w-20 h-7 rounded-sm  border-solid border-[#104EE9] bg-[#104EE9] text-white text-center text-sm font-normal'> Update </button>
                            </div>
                        </form>
                    )}
                </Formik>
            </div>
        </div>
    );
};

export default UpdateProductPage;
