import { gql } from "@apollo/client";

export const CREATE_PRODUCTWITHINGREDIENTS = gql`
  mutation CreateProductWithIngredients($data: ProductWithIngredientsInput!) {
    createProductWithIngredients(data: $data) {
      name
      id
      ingredients {
        name
      }
    }
  }
`;

export const GET_PRODUCTS = gql`
  query Products {
    products {
      id
      name
      description
      stock
    }
  }
`;

export const GET_PRODUCTSWITHINGREDIENTS = gql`
  query ProductsWithIngredients {
    productsWithIngredients {
      description
      name
      id
      stock
      ingredients {
        name
        id
      }
    }
  }
`;

export const DELETE_PRODUCT = gql`
  mutation DeleteProduct($deleteProductId: Float!) {
    deleteProduct(id: $deleteProductId)
  }
`;

export const UPDATE_PRODUCT = gql`
mutation UpdateProduct($input: ProductInput!, $updateProductId: Float!) {
    updateProduct(input: $input, id: $updateProductId) {
      description
      name
    }
  }
`;

export const UPDATE_PRODUCTWITHINGREDIENTS = gql`
mutation UpdateProductWithIngredients($data: ProductWithIngredientsInput!, $updateProductWithIngredientsId: Float!) {
    updateProductWithIngredients(data: $data, id: $updateProductWithIngredientsId) {
      description
      ingredients {
        name
      }
      name
      stock
    }
  }`;

  export const GET_LOWSTOCKPRODUCTS = gql`
  query LowStockProducts {
      lowStockProducts {
        name
        stock
      }
    }`;