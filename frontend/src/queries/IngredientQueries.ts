import { gql } from "@apollo/client";


export const GET_INGREDIENTS = gql`
query Ingredients {
    ingredients {
      name,
      id
    }
  }
`;


export const UPDATE_INGREDIENT = gql`
mutation UpdateIngredient($input: IngredientInput!, $updateIngredientId: Float!) {
    updateIngredient(input: $input, id: $updateIngredientId) {
      name
    }
  }
`;


export const DELETE_INGREDIENT = gql`
mutation DeleteIngredient($deleteIngredientId: Float!) {
    deleteIngredient(id: $deleteIngredientId)
  }`
  ;

export const GET_LOWSTOCKINGREDIENTS = gql`
    query LowStockIngredients {
      lowStockIngredients {
        name
        stock
      }
    }`;

export const CREATE_INGREDIENT = gql`
mutation CreateIngredient($ingredient: IngredientInput!) {
    createIngredient(Ingredient: $ingredient) {
      name
    }
  }
`;
